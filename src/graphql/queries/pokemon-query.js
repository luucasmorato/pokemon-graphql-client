import gql from "graphql-tag";

export const GET_ALL_POKEMON = gql`
  query GetAllPokemon($first: Int!) {
    pokemons(first: $first) {
      id
      name
      types
      image
    }
  }
`;
