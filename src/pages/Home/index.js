import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_ALL_POKEMON } from "../../graphql/queries/pokemon-query";

function Home() {
  const { loading, error, data } = useQuery(GET_ALL_POKEMON, {
    variables: {
      first: 10,
    },
  });

  if (loading) return <p>Carregando...</p>;
  if (error) return `${error.message}`;
  if (!data) return <p>Not found</p>;

  return <p>{console.log(data)}</p>;
}

export default Home;
